## Proof of concepts for various .NET technologies / libraries.

* Codility - various problems proposed on the [Codility](https://www.codility.com/) platform
* JaegerHelloWorld - a sample client for [Jaeger Distributed Tracing System](https://github.com/jaegertracing)
* LinqExamples - a suite of examples for LINQ operators
* NRules / NRules_RuleBuilder - a sample how to use [NRules](https://github.com/NRules/NRules), a business rule engine for .NET
* NTLMNancyOwin - a sample microservice using [NancyFx](https://github.com/NancyFx/Nancy) with OWIN and NTLM authentification
* NTMRestAPI - a sample ASP .NetCore microservice with NTLM authentification
* TestAsyncAwait - async / await example
* TestCircuitBreaker - implementation of the circuit breaker pattern
* TestEntityFramework - EntityFramework example
* TestMoq - [Moq](https://github.com/moq/moq4) is a friendly mocking framework for .NET
* TestNancyTopshelf - a sample microservice using [NancyFx](https://github.com/NancyFx/Nancy), Lightweight, low-ceremony, framework for building HTTP based services on .Net and Mono, and [Topshelf](https://github.com/Topshelf/Topshelf), An easy service hosting framework for building Windows services using .NET
* TestNewtonsoftJson - [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json) is a popular high-performance JSON framework for .NET
* TestNinject - [Ninject](https://github.com/ninject/Ninject) the ninja of .net dependency injectors
* TestPolly - [Polly](https://github.com/App-vNext/Polly) - is a .NET resilience and transient-fault-handling library that allows developers to express policies such as Retry, Circuit Breaker, Timeout, Bulkhead Isolation, and Fallback in a fluent and thread-safe manner
* TestRabbitMQ - [RabbitMQ](https://github.com/rabbitmq/rabbitmq-dotnet-client) client for RabbitMQ message broker
* TestSerilog - [Serilog](https://github.com/serilog/serilog) - Simple .NET logging with fully-structured events
* aspnetcore_docker - sample ASP .NetCore service with Dockerfile

